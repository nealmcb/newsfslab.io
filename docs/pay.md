# Software Freedom School

![SFS Logo](img/clean.jpeg)

## SFS: How To Pay         

How to pay for an SFS event
===========================

*   Put it in the Give Jar
*   [Bitcoin](/images/bitcoin.png):125TaTUWAzozYK8dWmXXvAzCsBr7eCWZUP
*   [Litecoin](/images/litecoin.png):LR6WNuNZ4bH18RbHqczkde8zrBG6MTMHoo
*   [PayPal](https://paypal.me/sfs303)
*   Take an Envelope of Karmic Justice
*   Ask for an [Invoice](/cdn-cgi/l/email-protection#84f4e5fde9e1eaf0f7c4f7ebe2f6e1e1aaf1f7)

What is Pay What You Choose
===========================

Some folks would rather not pay what we've asked. If you're reading this, you're probably one of them. For you, we have Pay What You Choose. You have all the freedom and responsibility now.

You Want to Pay More
--------------------

You love everything about SFS, and you want take make sure that our cupboards overflow. Thanks. We love you, too. We will gratefully recieve your generous gift at any of the usual places.

You Want to Pay Less
--------------------

You've never been to an SFS event, so you're not sure it'll be worth all that, so you'd like to pay some now and some later, if it turns out to be worth it. That's fine. We're happy to earn it. Come see.

You don't think the class is worth what we're asking. OK. We're cool with that, too. You pay what \*you\* think it's worth, but please let us know how we can improve, how we can live up to our high opinion of ourselves, so that we can be worth what we think we are.

You Want to Pay Differently
---------------------------

You don't have much USD. That's cool. We take all kinds of money. If it's money, we take it. Crypto, metals, foreign money, ancient money, stocks, bonds, you name it, we take it. The only thing we don't take is livestock. (gross exaggeration)

You don't have much money. That's cool. We take gifts of talent and time, too. Teach a class for us. Make us a llama painting. Help with writing, maintaining, and testing training material. And, thank you for your non-monetary trade. Often, these gifts mean more to us than money can.

You'd rather Pay It Forward. That's cool. Go volunteer at the local homeless shelter or animal rescue. Send us a letter and maybe some pictures. We'll treasure them.

Choose.
=======


### HQ Location:

4237 W. Grand Ave.
Denver, CO 80123


### Main phone number:

720-333-5267 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3072.8663027953703!2d-105.04331591974793!3d39.630214079564816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c8077c0a3b6db%3A0x9dfbc687ec2d9845!2sSoftware%20Freedom%20School!5e0!3m2!1sen!2sus!4v1639245108734!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

* David loves mkdocs
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
