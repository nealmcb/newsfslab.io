# Software Freedom School

![SFS Logo](img/clean.jpeg)

### Next Classes:

#### SFS Method (teach the teachers)

This course gives a Subject-Matter Expert (SME) a framework for delivery of technical training that will help all sorts of learners to both learn and enjoy the experience. The Time Pie, or Talk, Demo, Pair and Share is required for SME's who would like to teach with SFS.

#### Study Groups

Study groups are typically 8 or 16 weeks long and 2 or 4 hours each week. We've done study groups for RHCE, Python, Linux System Administration, Puppet, and Bash Programming.

#### Python Programming

This introduction to Python teaches the basics of programing.

#### Kubernetes

Container Management via Kubernetes. Another of our introductory classes, this one teaches the students how to use the Kubernetes software package.

#### Ansible

Taught by several different SFS teachers, Ansible is a fantastic way to manage servers and clusters. This class is usually targeted for beginners but we have the ability to offer intermediate and advanced Ansible classes.

#### Crypto-currency

High level overview of why to use it and how to get involved with the technology

#### Bash

Bash programing basics. We've taught bash in several different ways and forms from several different experienced teachers. If you'd like to request a Bash class, we can teach to all levels of students in several formats.

#### Security: Penetrations and Remediations

Taught annually by one of our favorite SFS partners, Penetrations and Remediations is a class for students of all levels. Teaching the student how to effectively defend their systems against outside threats makes for an excellent class.

#### Terraform

Infrastructure as code. Terraform is a great way to manage your Infrastructure. This class is taught to the beginner level using Google Cloud Compute Engine.

#### Docker

Another of our more popular classes, Docker is a great way to manage and use containers in you Infrastructure. This class can be taught at all levels.

#### SQL

This introduction to SQL teaches the student the basics of using SQL statements to search and manage data in SQL databases.

#### Linux

Linux is the cornerstone of the Internet these days. We teach Linux in several different forms, from 4 day bootcamps to weekly study groups. Again, we can teach this class to several different types of experience levels.

#### Continuous Integration

Learn Continuous Integration with Gitlab! This is one of our more popular classes, teach end users how to set up a continuous deployement using Gitlab.

#### Intro to DevOps

This Full-Day class teaches the basics of DevOps. From Linux to AWS, Gitlab to containers, all of the basics components of DevOps are included.

#### Puppet

Puppet is yet another orchastration tool used to setup and configure your servers.

#### GIT/Markdown

Git is a simple way to manage your code. The basics of Git and the Markdown utility are all tought in a fun 4 hour class.

#### Nextcloud, formerly ownCloud

Have you ever wanted to host your own Dropbox / Google Drive? With Nextcloud and ownCloud you can do just that! Teaching the basics of setting up your own cloud storage server, this class is one of our more popular classes.


### HQ Location:

4237 W. Grand Ave.
Denver, CO 80123


### Main phone number:

720-333-5267 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3072.8663027953703!2d-105.04331591974793!3d39.630214079564816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c8077c0a3b6db%3A0x9dfbc687ec2d9845!2sSoftware%20Freedom%20School!5e0!3m2!1sen!2sus!4v1639245108734!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
