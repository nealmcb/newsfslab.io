# Software Freedom School

![SFS Logo](img/clean.jpeg)

### SFS on the Internet

*   [Mattermost](https://mattermost.sofree.us)
*   [How to join Mattermost](/joinmm) (for the first time)
*   [GitLab.com/sofreeus](https://gitlab.com/sofreeus)
*   [How to pay for an SFS Event](/pay)
*   Feel free to open an issue or merge request for [this site](https://gitlab.com/sofreeus/newsfs.gitlab.io) if you have any suggestions.
*   [Sign-In Sheet](https://docs.google.com/forms/d/e/1FAIpQLSdsBq0iqsS8Th8SDaP37I-RAfvIGFnFv-0yKnE9vdRTTpiQTQ/viewform)
*   [Feedback Form](https://docs.google.com/forms/d/e/1FAIpQLSfJZ7sfXGyp3ruio522-ST_1peVEsjsEFKg4xMlrNIZwhTYUA/viewform?usp=sf_link)
*   [our email list](http://lists.sofree.us/cgi-bin/mailman/listinfo/sfs)


### HQ Location:

4237 W. Grand Ave.
Denver, CO 80123


### Main phone number:

720-333-5267 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3072.8663027953703!2d-105.04331591974793!3d39.630214079564816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c8077c0a3b6db%3A0x9dfbc687ec2d9845!2sSoftware%20Freedom%20School!5e0!3m2!1sen!2sus!4v1639245108734!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
