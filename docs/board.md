# Software Freedom School

![SFS Logo](img/clean.jpeg)

### The Core SFS Team

| Name | Contact Info | About |
|-----|-----|-----|
| _David L. Willson_ | dlwillson (at) sofree (dot) us | The starry-eyed pirate captain who turned the twinkle in his eye into the Software Freedom School in 2013. David has over two decades of experience as a Linux sysadmin and has appeared across the continent at events supporting free software. |
| _Heather_ | hlwillson (at) sofree (dot) us  | It is good to be David's wife and co-founder of SFS. There are many things I contribute to SFS but my favorite is feeding the geeks. It is a pleasure to serve my friends and be in an community that truly takes care of and supports one another.|
| _Aaron_ | aayore (at) sofree (dot) us  | Aaron is part of the original crew of SFS. He brings DevOps, leadership, and a desire to make our community great! |

### HQ Location:

4237 W. Grand Ave.
Denver, CO 80123

### Main phone number:

720-333-5267 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3072.8663027953703!2d-105.04331591974793!3d39.630214079564816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c8077c0a3b6db%3A0x9dfbc687ec2d9845!2sSoftware%20Freedom%20School!5e0!3m2!1sen!2sus!4v1639245108734!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
