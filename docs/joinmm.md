# Software Freedom School

![SFS Logo](img/clean.jpeg)

 How to join Mattermost

How to join SFS Mattermost
==========================

Browse to [https://mattermost.sofree.us](https://mattermost.sofree.us)

To register and authenticate with GitLab.com, click "GitLab"

If you'd rather register with your email address, see the next section.

![screen](img/join-mm-screen-1.png)

Click "Authorize"

![screen](img/join-mm-screen-2.png)

Click "SFS303"

![screen](img/join-mm-screen-3.png)

Click "Next" or "Skip tutorial"

![screen](img/join-mm-screen-4.png)

Optionally, DM somebody.

*   dlwillson, the creator of these terrific instructions

![screen](img/join-mm-screen-5.png)

* * *

How to join SFS Mattermost by email
===================================

Browse to [https://mattermost.sofree.us](https://mattermost.sofree.us)

Click "Create one now."

![screen](img/join-mm-screen-e1.png)

Click "Email and Password"

![screen](img/join-mm-screen-e2.png)

Fill out the form with your email address, username, and password.

![screen](img/join-mm-screen-e3.png)

Verify your email address by clicking the link in the email message you get.

(varies)

Click "Sign in"

![screen](img/join-mm-screen-e4.png)

### HQ Location:

4237 W. Grand Ave.
Denver, CO 80123


### Main phone number:

720-333-5267 

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3072.8663027953703!2d-105.04331591974793!3d39.630214079564816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876c8077c0a3b6db%3A0x9dfbc687ec2d9845!2sSoftware%20Freedom%20School!5e0!3m2!1sen!2sus!4v1639245108734!5m2!1sen!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
