# newsfs.gitlab.io

New SFS Website


## Getting started:

The new SFS Website is made using [mkdocs](https://mkdocs.org). There are no fancy CSS or templates that have to be used to modify the website. Mkdocs simply converst markdown documents into pages. View the mkdocs website for greater information. This README will only cover how to use and modify what we have currently set up.

## Installing mkdocs:


`sudo apt install mkdocs`
or
`sudo pip install mkdocs`
or
`sudo dnf install mkdocs`

## Using mkdocs:

Using mkdocs is super simple.

`mkdocs new <sitename>` creates a new website.

`mkdocs serve` runs the current docs on https://localhost:8000

#### Note: When `mkdocs serve` is being run, any changes to the markdown files will be published to the https://localhost:8000 everytime the file is updated or saved.

## Making changes to the sfs static website

The current theme used is the Material theme. It needs to be installed before doing anything else.

`pip install mkdocs-material`

Next, clone this repository

`git clone git@gitlab.com:sofreeus/newsfs.gitlab.io.git`

Move into the new repo with

`cd newsfs.gitlab.io.git`

That leaves us with the following files:

```bash
.
├── README.md
├── docs
│   ├── about.md
│   ├── board.md
│   ├── class.md
│   ├── img
│   │   └── sfs-300x300.png
│   ├── index.md
│   └── links.md
├── img
│   └── favicon.ico
├── mkdocs.yml
├── public
│   ├── 404.html
│   ├── about
│   │   └── index.html
│   ├── assets
│   │   ├── images
│   │   │   └── favicon.png
│   │   ├── javascripts
│   │   │   ├── bundle.17f42bbf.min.js
│   │   │   ├── bundle.17f42bbf.min.js.map
│   │   │   └── workers
│   │   │       ├── search.cefbb252.min.js
│   │   │       └── search.cefbb252.min.js.map
│   │   └── stylesheets
│   │       ├── main.82f3c0b9.min.css
│   │       ├── main.82f3c0b9.min.css.map
│   │       ├── palette.9204c3b2.min.css
│   │       └── palette.9204c3b2.min.css.map
│   ├── board
│   │   └── index.html
│   ├── class
│   │   └── index.html
│   ├── img
│   │   └── sfs-300x300.png
│   ├── index.html
│   ├── links
│   │   └── index.html
│   ├── search
│   │   └── search_index.json
│   ├── sitemap.xml
│   └── sitemap.xml.gz


...

```

All of the pages that make up the current website exist in the ./docs folder. To modify them, simply use your favorite markdown editor to edit and save. Once your changes are done, you can preview them at http://localhost:8000 with `mkdocs serve`


Now simply commit your changes in git and push to the repo.

```bash

git add .
git commit
git push

```

## To add a completely new page:

Adding a new page is easy but it requires a few extra steps.

1. Add the new page to the mkdocs.yml file:

```yaml

site_name: Software Freedom School
site_url: https://sofree.us
nav:
        - Home: index.md
        - Board of Directors: board.md
        - About SFS: about.md
        - Links: links.md
        - Class Catalog: class.md

theme:
        name: material

```

2. Simply add the new page under the `nav:` section of the yaml file.
3. Create the new page as <page>.md in the ./docs folder.

Then follow the same steps as above to push the new website repo.

The site is build automagically in GitLab so after pushing changes, be sure to confirm the site is up and working.

